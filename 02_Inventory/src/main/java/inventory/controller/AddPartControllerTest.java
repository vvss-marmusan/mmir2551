package inventory.controller;

import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class AddPartControllerTest {


    private InventoryRepository repo = new InventoryRepository();
    private InventoryService service = new InventoryService(repo);
    private AddPartController addPartController = new AddPartController();
    private static final String[] name = new String[8];
    private static final Double[] price = new Double[8];
    private static int[] inStock = new int[8];
    private static int[] min = new int[8];
    private static int[] max = new int[8];
    private static String[] idSauFabrica = new String[8];

    {
        this.addPartController.setService(service);
    }

    @BeforeAll
    static void initialization() {

        name[0] = "part name";
        price[0] = 100.0;
        inStock[0] = 10;
        min[0] = 5;
        max[0] = 200;
        idSauFabrica[0] = "17849";

        name[1] = "part name";
        price[1] = 5.0;
        inStock[1] = 12;
        min[1] = 1;
        max[1] = 100;
        idSauFabrica[1] = "SRL";

        name[2] = "a";
        price[2] = 1.0;
        min[2] = 1;
        inStock[2] = min[2] + 1;
        max[2] = 2;
        idSauFabrica[2] = "2";

        name[3] = "a";
        price[3] = Double.MAX_VALUE - 2;
        min[3] = 1;
        inStock[3] = min[3] + 1;
        max[3] = Integer.MAX_VALUE - 2;
        idSauFabrica[3] = "SRL";

        name[4] = "part name";
        price[4] = 10.0;
        min[4] = 2;
        inStock[4] = -5;
        max[4] = 20;
        idSauFabrica[4] = "7483";

        name[5] = "part name";
        price[5] = -1.0;
        min[5] = 5;
        inStock[5] = 10;
        max[5] = 10;
        idSauFabrica[5] = "La Crocodil SRL";

        name[6] = "a";
        price[6] = 0.0;
        min[6] = 1;
        inStock[6] = min[6] + 1;
        max[6] = 2;
        idSauFabrica[6] = "2";

        name[7] = "a";
        price[7] = 1.0;
        max[7] = Integer.MAX_VALUE - 2;
        min[7] = max[7] + 1;
        inStock[7] = min[7] + 1;
        idSauFabrica[7] = "La crocodil SRL";
    }

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @Disabled
    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.params.ParameterizedTest
    @ValueSource(ints = {0, 1})
    @DisplayName("Teste Valide ECP")
    @Tag("ECP-VALID")
    void addPartIntermediaryEcpValid(int i) {
        int repoSize = repo.getAllParts().size();
        try {
            addPartController.addPartIntermediary(name[i], price[i], inStock[i], min[i], max[i], idSauFabrica[i]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert repo.getAllParts().size() == repoSize+1;
    }

    @org.junit.jupiter.params.ParameterizedTest
    @ValueSource(ints = {2, 3})
    @DisplayName("Teste Valide BVA")
    @Tag("BVA-VALID")
    void addPartIntermediaryBvaValid(int i) {
        int repoSize = repo.getAllParts().size();
        try {
            addPartController.addPartIntermediary(name[i], price[i], inStock[i], min[i], max[i], idSauFabrica[i]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert repo.getAllParts().size() == repoSize+1;
    }

    @org.junit.jupiter.params.ParameterizedTest
    @ValueSource(ints = {4})
    @DisplayName("Teste Nonvalide ECP")
    @Tag("ECP-INVALID")
    void addPartIntermediaryEcpInvalid(int i) {
        int repoSize = repo.getAllParts().size();
        try {
            addPartController.addPartIntermediary(name[i], price[i], inStock[i], min[i], max[i], idSauFabrica[i]);
        } catch (Exception e) {
            assertEquals("numarul de bucati trebuie sa fie strict pozitiv; numarul de bucati trebuie sa fie mai mare ca minimul; ", e.getMessage(), "not ok");
        }
        assert repo.getAllParts().size() == repoSize;
    }


    @org.junit.jupiter.params.ParameterizedTest
    @ValueSource(ints = {5})
    @DisplayName("Teste Nonvalide ECP")
    @Tag("ECP-INVALID")
    void addPartIntermediaryEcpInvalid2(int i) {
        int repoSize = repo.getAllParts().size();
        try {
            addPartController.addPartIntermediary(name[i], price[i], inStock[i], min[i], max[i], idSauFabrica[i]);
        } catch (Exception e) {
            assertEquals("pretul trebuie sa fie strict pozitiv; ", e.getMessage(), "not ok");
        }
        assert repo.getAllParts().size() == repoSize;
    }


    @org.junit.jupiter.params.ParameterizedTest
    @ValueSource(ints = {6})
    @Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    @DisplayName("Teste Nonvalide BVA")
    @Tag("BVA-INVALID")
    void addPartIntermediaryBvaInvalid(int i) {
        int repoSize = repo.getAllParts().size();
        try {
            addPartController.addPartIntermediary(name[i], price[i], inStock[i], min[i], max[i], idSauFabrica[i]);
        } catch (Exception e) {
            assertEquals("pretul trebuie sa fie strict pozitiv; ", e.getMessage(), "not ok");
        }
        assert repo.getAllParts().size() == repoSize;
    }


    @org.junit.jupiter.params.ParameterizedTest
    @ValueSource(ints = {7})
    @Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    @DisplayName("Teste Nonvalide BVA")
    @Tag("BVA-INVALID")
    void addPartIntermediaryBvaInvalid2(int i) {
        int repoSize = repo.getAllParts().size();
        try {
            addPartController.addPartIntermediary(name[i], price[i], inStock[i], min[i], max[i], idSauFabrica[i]);
        } catch (Exception e) {
            assertEquals("minimul trebuie sa fie mai mic ca maximul; numarul de bucati trebuie sa fie mai mic ca maximul; ", e.getMessage(), "not ok");
        }
        assert repo.getAllParts().size() == repoSize;
    }


}