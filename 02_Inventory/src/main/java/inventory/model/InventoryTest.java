package inventory.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {

    private Inventory inventory= new Inventory();

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testCase1() {
        inventory.setAllParts(null);
        assertNull(inventory.lookupPart(null));
    }
    @Test
    void testCase2() {
        inventory.setAllParts(null);
        ObservableList<Part> l= FXCollections.observableArrayList();
        l.add(new InhousePart(1,"surub",1,1,1,1,1));
        inventory.setAllParts(l);
        assertEquals(inventory.lookupPart("surub").getName(),"surub");
    }
    @Test
    void testCase3() {
        inventory.setAllParts(null);
        ObservableList<Part> l= FXCollections.observableArrayList();
        inventory.setAllParts(l);
        assertNull(inventory.lookupPart("surub"));
    }
    @Test
    void testCase4() {
        inventory.setAllParts(null);
        ObservableList<Part> l= FXCollections.observableArrayList();
        l.add(new InhousePart(1,"piulita",1,1,1,1,1));
        l.add(new InhousePart(2,"surub",1,1,1,1,1));
        l.add(new InhousePart(3,"cui",1,1,1,1,1));
        inventory.setAllParts(l);
        assertEquals(inventory.lookupPart("surub").getName(),"surub");

    }
    @Test
    void testCase5() {
        inventory.setAllParts(null);
        ObservableList<Part> l= FXCollections.observableArrayList();
        l.add(new InhousePart(1,"surub",1,1,1,1,1));
        inventory.setAllParts(l);
        assertNull(inventory.lookupPart("piulita"));
    }
    @Test
    void testCase6() {
        inventory.setAllParts(null);
        ObservableList<Part> l= FXCollections.observableArrayList();
        l.add(new InhousePart(125,"surub",1,1,1,1,1));
        inventory.setAllParts(l);
        assertEquals(inventory.lookupPart("125").getPartId(),125);

    }
}